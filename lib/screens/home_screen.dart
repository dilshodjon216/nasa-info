import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nasa_api/model/nasa_model.dart';
import 'package:http/http.dart' as http;
import 'package:nasa_api/widgets/error.dart';
import 'package:nasa_api/widgets/nasa_card.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<List<Nasa>> nasalist;
  final String uri = "https://apodapi.herokuapp.com/api/?count=25";

  Future<List<Nasa>> fetchNasa() async {
    var response = await http.get(uri);

    if (response.statusCode == 200) {
      final jsonList = jsonDecode(response.body);
      if (jsonList is List) {
        return jsonList.map((json) => Nasa.fromJson(json)).toList();
      }
    }
    throw Exception("Http call not made");
  }

  @override
  void initState() {
    super.initState();
    nasalist = fetchNasa();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text(
          "Infinity",
          style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
        ),
        elevation: 16.0,
        centerTitle: true,
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.autorenew),
              onPressed: () {
                setState(() {
                  nasalist = fetchNasa();
                });
              }),
        ],
      ),
      body: FutureBuilder(
        future: nasalist,
        builder: (BuildContext context, AsyncSnapshot<List<Nasa>> snapshot) {
          if (snapshot.hasData) {
            return ListView(
              padding: EdgeInsets.all(16.0),
              children:
                  snapshot.data.map((nasa) => NasaCard(nasa: nasa)).toList(),
            );
          } else if (snapshot.hasError) {
            return buildErrorWidget(snapshot.hasError.toString());
          } else {
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.blue),
              ),
            );
          }
        },
      ),
    );
  }
}
