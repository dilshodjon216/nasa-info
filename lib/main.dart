import 'package:flutter/material.dart';
import 'package:nasa_api/screens/home_screen.dart';

void main() => runApp(Infinity());


class Infinity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Infinity',
      theme: ThemeData(
        primaryColor: Colors.blue
      ),
      home: HomeScreen(),
    );
  }
}