import 'dart:convert';

class Nasa {
  final String apod_site;
  final String copyright;
  final String date;
  final String description;
  final String hdurl;
  final String image_thumbnail;
  final String media_type;
  final String title;
  final String url;

  Nasa({
    this.apod_site,
    this.copyright,
    this.date,
    this.description,
    this.hdurl,
    this.image_thumbnail,
    this.media_type,
    this.title,
    this.url,
  });

  Nasa.fromJson(Map<String, dynamic> json)
      : apod_site = json["apod_site"],
        copyright = json["copyright"],
        date = json["date"],
        description = json["description"],
        hdurl = json["hdurl"],
        image_thumbnail = json["image_thumbnail"],
        media_type = json["media_type"],
        title = json["title"],
        url = json["url"];
}
